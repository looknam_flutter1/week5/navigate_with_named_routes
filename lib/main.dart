import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Route Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => const FirstScreen(),
      '/second': (context) => const SecondScreen(),
      '/third': (context) => const ThirdScreen()
    },
  ));
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('First Screen'),
        ),
        body: Center(
            child: Column(
          children: [
            SizedBox(height: 8.0),
            ElevatedButton(
              child: const Text('Launch Second Screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/second');
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton(
              child: const Text('Launch Third Screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/third');
              },
            ),
            SizedBox(height: 8.0),
          ],
        )));
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Second Screen'),
        ),
        body: Center(
          child: ElevatedButton(
            child: const Text('Go Back'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ));
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Third Screen'),
        ),
        body: Center(
            child: Column(
          children: [
            SizedBox(height: 8.0),
            ElevatedButton(
              child: const Text('Go to Second Screen'),
              onPressed: () {
                Navigator.pushNamed(context, '/second');
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton(
              child: const Text('Go Back'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        )));
  }
}
